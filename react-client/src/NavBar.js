

import React from 'react';
import {Redirect} from "react-router-dom";
import {Navbar, Nav, Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import {logout, refreshPage, loggedIn, decodeToken, getTheme} from './services/userServices';
import logo from "./images/logo.svg"

class AppNavBar extends React.Component{
    constructor(props){
      super(props);
      this.state ={
        isLogin : false,
        isLoad : false,
        theme :"",
        bodyStyle : {
          backgroundColor: "none"          
        },
        fontColor:{
          color:"black"
        }
      }  
    }
  
    componentDidMount(){
      if(loggedIn()) this.setState({"isLogin":true});
      else this.setState({"isLogin":false});

      if(getTheme()!== null)this.setState({theme:getTheme()})
      else this.setState({theme:"light"})


      if(getTheme() === "dark") 
      {

        this.setState({bodyStyle:{
            backgroundColor: "#1D0857",
            color:"white"
        }});
        this.setState({fontColor:{
          color:"white"
        }})
      }else{
        
        this.setState({bodyStyle:{
            background: "none"
        }})
        this.setState({fontColor:{
          color:"black"
        }})
      }
    }
  
    onLogout = ()=>{
      logout();
      this.setState({"isLogin":false});
      refreshPage();
      console.log("out")
      return <Redirect to='/' />
    }
  
    userNavBar=()=>{
      let data = decodeToken();
      return(
        <Nav>
          <Nav.Link href="/" style={this.state.fontColor}>{data.name}</Nav.Link>
          <Nav.Link onClick={this.onLogout} style={this.state.fontColor}>Logout</Nav.Link>
        </Nav>
      )
    }
  
    visiterNavBar= ()=>{
      return(
        <Nav >
        <Nav.Link href="/auth/Login" style={this.state.fontColor}>Login</Nav.Link>
        <Nav.Link href="/auth/register" style={this.state.fontColor}>Register</Nav.Link>
      </Nav>
      )  
    }
    handleTheme =(event)=>{
      if(this.state.theme === "dark") this.setState({theme: "light"}); 
      else  this.setState({theme: "dark"});  
      refreshPage();  
    }


    componentDidUpdate(){
      localStorage.setItem("theme", this.state.theme);
      
      console.log(this.state.theme)
    }
  
    render(){
      return(
        <Navbar className="navbar" collapseOnSelect expand="lg" style={this.state.bodyStyle}>      
          <Navbar.Brand style={this.state.fontColor} href="/"><img src={logo} alt="logo" style={{width:"25px", height:"25px"}}/> Minimis</Navbar.Brand>  
          <Form>
            {(this.state.theme === "dark")?
              <Form.Check onChange={this.handleTheme}
                type="switch"
                name="toggle"
                id="custom-switch"
                label="Dark" checked/>
              :
              <Form.Check onChange={this.handleTheme}
                type="switch"
                name="toggle"
                id="custom-switch"
                label="Dark"/>
            }           
          </Form>     
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto"></Nav>
            {this.state.isLogin?this.userNavBar():this.visiterNavBar()}
          </Navbar.Collapse>
        </Navbar>           
      );
    }
  }

  export default AppNavBar;
import mongoose from 'mongoose';
import {register, login, getUser, withAuth, updateUser} from '../controllers/userController';
import jwt from 'jsonwebtoken';
import decode from 'jwt-decode';
const user = mongoose.model('users');



module.exports = (app) => {
  app.post(`/api/register`, register)

  app.post(`/api/login`,login)

  app.get(`/api/user`,withAuth, getUser)

  app.put(`/api/user`, updateUser)

  app.get(`/api/test`, (req,res)=>{
    res.sendStatus(200)
  })

}